module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        nodemon: {
            dev: {
                script: "bin/www",
                options: {
                    nodeArgs: ["--debug"]
                }
            }
        },
        concurrent: {
            debug: {
                tasks: ['nodemon'],
                options: {
                    logConcurrentOutput: true
                }
            }
        }


    });

    /*grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');*/
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-nodemon');

    grunt.registerTask('debug', ['concurrent:debug']);
    /*grunt.registerTask('default', ['concat', 'uglify']);*/
};